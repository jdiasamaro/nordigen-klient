package com.gitlab.jdiasamaro.nordigen.core

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.containsOnly
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.github.tomakehurst.wiremock.http.RequestMethod.DELETE
import com.github.tomakehurst.wiremock.http.RequestMethod.GET
import com.github.tomakehurst.wiremock.http.RequestMethod.POST
import com.gitlab.jdiasamaro.nordigen.BaseNordigenTest
import com.gitlab.jdiasamaro.nordigen.model.Requisition
import com.gitlab.jdiasamaro.nordigen.model.RequisitionStatus.GRANTING_ACCESS
import com.gitlab.jdiasamaro.nordigen.model.RequisitionStatus.LINKED
import org.junit.Test
import java.time.Instant

class NordigenRequisitionsV2Test: BaseNordigenTest() {

    private val dummyRequisitions = listOf(Requisition(
        id = "req-1",
        reference = "ref-1",
        ssn = "ssn-1",
        status = LINKED,
        accountSelection = false,
        userLanguage = "PT",
        oauthAuthorizationUrl = "https://oauth.nordigen.com",
        accountIds = listOf("acc-1", "acc-2"),
        institutionId = "OPEN_BANKING_TEST_BANK",
        redirectUrl = "https://my.url.com",
        created = Instant.parse("2021-11-29T18:43:35Z"),
        endUserAgreementId = "agree-1"
    ), Requisition(
        id = "req-2",
        status = GRANTING_ACCESS,
        accountSelection = true,
        oauthAuthorizationUrl = "https://oauth.nordigen.com",
        accountIds = listOf("acc-3"),
        institutionId = "OPEN_BANKING_TEST_BANK",
        redirectUrl = "https://my.url.com",
        created = Instant.parse("2021-11-29T18:43:35Z")
    ))

    private fun mockDummyRequisitions() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "count" to 20,
                "results" to listOf(mapOf(
                    "id" to "req-1",
                    "reference" to "ref-1",
                    "ssn" to "ssn-1",
                    "status" to "LN",
                    "account_selection" to false,
                    "user_language" to "PT",
                    "link" to "https://oauth.nordigen.com",
                    "accounts" to listOf("acc-1", "acc-2"),
                    "institution_id" to "OPEN_BANKING_TEST_BANK",
                    "redirect" to "https://my.url.com",
                    "created" to "2021-11-29T18:43:35Z",
                    "agreement" to "agree-1"
                ), mapOf(
                    "id" to "req-2",
                    "status" to mapOf(
                        "short" to "GA",
                        "long" to "GRANTING_ACCESS",
                        "description" to "useless"),
                    "account_selection" to true,
                    "link" to "https://oauth.nordigen.com",
                    "accounts" to listOf("acc-3"),
                    "institution_id" to "OPEN_BANKING_TEST_BANK",
                    "redirect" to "https://my.url.com",
                    "created" to "2021-11-29T18:43:35Z"
                ))
            ))
        }
    }

    @Test
    fun `NordigenKlient is able to return a list of all requisitions`() {
        mockDummyRequisitions()

        val requisitions = mockedNordigenKlient().v2.requisitions.getAll()

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/requisitions/")
        }

        assertThat(requisitions.count).isEqualTo(20)
        assertThat(requisitions.currentPage).hasSize(2)
        assertThat(requisitions.currentPage).containsExactlyInAnyOrder(dummyRequisitions[0], dummyRequisitions[1])
    }

    @Test
    fun `NordigenKlient is able to return a list of all requisitions using both limit and offset`() {
        mockDummyRequisitions()

        val requisitions = mockedNordigenKlient().v2.requisitions.getAll(limit = 123, offset = 456)

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/requisitions/?limit=123&offset=456")
            assertThat(queryParams).hasSize(2)
            assertThat(queryParams.getValue("limit").values()).containsOnly("123")
            assertThat(queryParams.getValue("offset").values()).containsOnly("456")
        }

        assertThat(requisitions.count).isEqualTo(20)
        assertThat(requisitions.currentPage).hasSize(2)
        assertThat(requisitions.currentPage).containsExactlyInAnyOrder(dummyRequisitions[0], dummyRequisitions[1])
    }

    @Test
    fun `NordigenKlient is able to return a requisitions based on its ID`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "id" to "req-2",
                "status" to mapOf(
                    "short" to "GA",
                    "long" to "GRANTING_ACCESS",
                    "description" to "useless"),
                "account_selection" to true,
                "link" to "https://oauth.nordigen.com",
                "accounts" to listOf("acc-3"),
                "institution_id" to "OPEN_BANKING_TEST_BANK",
                "redirect" to "https://my.url.com",
                "created" to "2021-11-29T18:43:35Z"
            ))
        }

        val requisition = mockedNordigenKlient().v2.requisitions.get("req-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/requisitions/req-id/")
        }

        assertThat(requisition).isEqualTo(dummyRequisitions[1])
    }

    @Test
    fun `NordigenKlient is able to delete a requisitions based on its ID`() {
        mockHttpResponse {
            statusCode = 200
        }

        mockedNordigenKlient().v2.requisitions.delete("req-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(DELETE)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/requisitions/req-id/")
        }
    }

    @Test
    fun `NordigenKlient is able to create a requisitions`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "id" to "req-2",
                "status" to mapOf(
                    "short" to "GA",
                    "long" to "GRANTING_ACCESS",
                    "description" to "useless"),
                "account_selection" to true,
                "link" to "https://oauth.nordigen.com",
                "accounts" to listOf("acc-3"),
                "institution_id" to "OPEN_BANKING_TEST_BANK",
                "redirect" to "https://my.url.com",
                "created" to "2021-11-29T18:43:35Z"
            ))
        }

        val requisition = mockedNordigenKlient().v2.requisitions.create(
            institutionId = "i-id",
            redirectUrl = "https://redirect.com",
            reference = "ref",
            userLanguage = "PT",
            endUserAgreementId = "agreement"
        )

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(POST)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/requisitions/")
            fromJsonObjectString(bodyAsString).apply {
                assertThat(this!!.keys).containsExactlyInAnyOrder(
                    "institution_id",
                    "redirect",
                    "reference",
                    "user_language",
                    "agreement")

                assertThat(get("institution_id")).isEqualTo("i-id")
                assertThat(get("redirect")).isEqualTo("https://redirect.com")
                assertThat(get("reference")).isEqualTo("ref")
                assertThat(get("user_language")).isEqualTo("PT")
                assertThat(get("agreement")).isEqualTo("agreement")
            }
        }

        assertThat(requisition).isEqualTo(dummyRequisitions[1])
    }

    @Test
    fun `NordigenKlient handles HTTP errors for all requisition requests`() {
        val requestFunctions = listOf(
            { mockedNordigenKlient().v2.requisitions.getAll(1, 2) },
            { mockedNordigenKlient().v2.requisitions.getAll() },
            { mockedNordigenKlient().v2.requisitions.get("req-id") },
            { mockedNordigenKlient().v2.requisitions.delete("req-id") },
            { mockedNordigenKlient().v2.requisitions.create(
                institutionId = "i-id",
                redirectUrl = "https://redirect.com",
                reference = "ref",
                userLanguage = "PT",
                endUserAgreementId = "agreement"
            ) }
        )

        assertNordigenApiExceptionIsThrownWithPartialInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithFullInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithNoInfo(requestFunctions)
    }
}