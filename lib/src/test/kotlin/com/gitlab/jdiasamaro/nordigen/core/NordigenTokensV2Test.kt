package com.gitlab.jdiasamaro.nordigen.core

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.anyUrl
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.http.RequestMethod.POST
import com.gitlab.jdiasamaro.nordigen.BaseNordigenTest
import org.junit.Test

class NordigenTokensV2Test: BaseNordigenTest() {

    @Test
    fun `NordigenKlient is able to create a new access token`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "access" to "new-access-token",
                "access_expires" to 1000,
                "refresh" to "new-refresh-token",
                "refresh_expires" to 5000,
            ))
        }

        val accessToken = mockedNordigenKlient().v2.tokens.create("dummy-id", "dummy-key")

        verify(postRequestedFor(anyUrl())) {
            assertThat(method).isEqualTo(POST)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/token/new/")
            fromJsonObjectString(bodyAsString).apply {
                assertThat(this!!.keys).containsExactlyInAnyOrder("secret_id", "secret_key")
                assertThat(get("secret_id")).isEqualTo("dummy-id")
                assertThat(get("secret_key")).isEqualTo("dummy-key")
            }
        }

        assertThat(accessToken.accessToken).isEqualTo("new-access-token")
        assertThat(accessToken.accessTokenExpireSeconds).isEqualTo(1000)
        assertThat(accessToken.refreshToken).isEqualTo("new-refresh-token")
        assertThat(accessToken.refreshTokenExpireSeconds).isEqualTo(5000)
    }

    @Test
    fun `NordigenKlient is able to refresh an access token`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "access" to "another-access-token",
                "access_expires" to 1234,
            ))
        }

        val accessToken = mockedNordigenKlient().v2.tokens.refresh("dummy-refresh")

        verify(postRequestedFor(anyUrl())) {
            assertThat(method).isEqualTo(POST)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/token/refresh/")
            fromJsonObjectString(bodyAsString).apply {
                assertThat(this!!.keys).containsExactlyInAnyOrder("refresh")
                assertThat(get("refresh")).isEqualTo("dummy-refresh")
            }
        }

        assertThat(accessToken.accessToken).isEqualTo("another-access-token")
        assertThat(accessToken.accessTokenExpireSeconds).isEqualTo(1234)
    }

    @Test
    fun `NordigenKlient handles HTTP errors for all token requests`() {
        val requestFunctions = listOf(
            { mockedNordigenKlient().v2.tokens.create("id", "key") },
            { mockedNordigenKlient().v2.tokens.refresh("refresh") },
        )

        assertNordigenApiExceptionIsThrownWithPartialInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithFullInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithNoInfo(requestFunctions)
    }
}