package com.gitlab.jdiasamaro.nordigen.core

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.containsOnly
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import assertk.assertions.startsWith
import com.github.tomakehurst.wiremock.http.RequestMethod.GET
import com.gitlab.jdiasamaro.nordigen.BaseNordigenTest
import com.gitlab.jdiasamaro.nordigen.model.Institution
import org.junit.Test

class NordigenInstitutionsV2Test: BaseNordigenTest() {

    @Test
    fun `NordigenKlient is able to return a list of all institutions for all countries`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonArrayString(listOf(mapOf(
                "id" to "NORDIGEN_TEST_BANK",
                "name" to "Nordigen Test Bank Inc.",
                "countries" to listOf("PT", "GB"),
                "logo" to "https://cdn.nordigen.com/somelogo.png",
                "transaction_total_days" to 180,
            ), mapOf(
                "id" to "OPEN_BANKING_TEST_BANK",
                "name" to "European Open Bank",
                "bic" to "OPEN_BANK_XX123",
                "countries" to listOf("DE", "BE"),
                "logo" to "https://cdn.nordigen.com/openbanking.png"
            )))
        }

        val institutions = mockedNordigenKlient().v2.institutions.getAll()

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/institutions/")
        }

        assertThat(institutions).hasSize(2)
        assertThat(institutions).containsExactlyInAnyOrder(Institution(
            id = "NORDIGEN_TEST_BANK",
            name = "Nordigen Test Bank Inc.",
            countries = listOf("PT", "GB"),
            logoUrl = "https://cdn.nordigen.com/somelogo.png",
            transactionTotalDays = 180
        ), Institution(
            id = "OPEN_BANKING_TEST_BANK",
            name = "European Open Bank",
            bic = "OPEN_BANK_XX123",
            countries = listOf("DE", "BE"),
            logoUrl = "https://cdn.nordigen.com/openbanking.png",
        ))
    }

    @Test
    fun `NordigenKlient is able to return a list of institutions filtered by country`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonArrayString(listOf(mapOf(
                "id" to "NORDIGEN_TEST_BANK",
                "name" to "Nordigen Test Bank Inc.",
                "countries" to listOf("PT", "GB"),
                "logo" to "https://cdn.nordigen.com/somelogo.png",
                "transaction_total_days" to 180,
            ), mapOf(
                "id" to "OPEN_BANKING_TEST_BANK",
                "name" to "European Open Bank",
                "bic" to "OPEN_BANK_XX123",
                "countries" to listOf("DE", "BE"),
                "logo" to "https://cdn.nordigen.com/openbanking.png"
            )))
        }

        val institutions = mockedNordigenKlient().v2.institutions.getAll(country = "PT")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).startsWith("${endpoint}/institutions/?")
            assertThat(queryParams).hasSize(1)
            assertThat(queryParams.getValue("country").values()).containsOnly("PT")
        }

        assertThat(institutions).hasSize(2)
        assertThat(institutions).containsExactlyInAnyOrder(Institution(
            id = "NORDIGEN_TEST_BANK",
            name = "Nordigen Test Bank Inc.",
            countries = listOf("PT", "GB"),
            logoUrl = "https://cdn.nordigen.com/somelogo.png",
            transactionTotalDays = 180
        ), Institution(
            id = "OPEN_BANKING_TEST_BANK",
            name = "European Open Bank",
            bic = "OPEN_BANK_XX123",
            countries = listOf("DE", "BE"),
            logoUrl = "https://cdn.nordigen.com/openbanking.png",
        ))
    }

    @Test
    fun `NordigenKlient is able to return details of an institution based on its ID`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "id" to "NORDIGEN_TEST_BANK",
                "name" to "Nordigen Test Bank Inc.",
                "countries" to listOf("PT", "GB"),
                "logo" to "https://cdn.nordigen.com/somelogo.png",
                "transaction_total_days" to 180,
            ))
        }

        val institution = mockedNordigenKlient().v2.institutions.get("NORDIGEN_TEST_BANK")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/institutions/NORDIGEN_TEST_BANK/")
        }

        assertThat(institution).isEqualTo(Institution(
            id = "NORDIGEN_TEST_BANK",
            name = "Nordigen Test Bank Inc.",
            countries = listOf("PT", "GB"),
            logoUrl = "https://cdn.nordigen.com/somelogo.png",
            transactionTotalDays = 180
        ))
    }

    @Test
    fun `NordigenKlient handles HTTP errors for all institution requests`() {
        val requestFunctions = listOf(
            { mockedNordigenKlient().v2.institutions.getAll("PT") },
            { mockedNordigenKlient().v2.institutions.getAll() },
            { mockedNordigenKlient().v2.institutions.get("i-id") },
        )

        assertNordigenApiExceptionIsThrownWithPartialInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithFullInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithNoInfo(requestFunctions)
    }
}