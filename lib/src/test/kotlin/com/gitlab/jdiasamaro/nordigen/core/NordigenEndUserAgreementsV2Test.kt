package com.gitlab.jdiasamaro.nordigen.core

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.containsOnly
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import assertk.assertions.startsWith
import com.github.tomakehurst.wiremock.http.RequestMethod.DELETE
import com.github.tomakehurst.wiremock.http.RequestMethod.GET
import com.github.tomakehurst.wiremock.http.RequestMethod.POST
import com.github.tomakehurst.wiremock.http.RequestMethod.PUT
import com.gitlab.jdiasamaro.nordigen.BaseNordigenTest
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreement
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAcceptanceInfo
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScope.BALANCES
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScope.DETAILS
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScope.TRANSACTIONS
import org.junit.Test
import java.time.Instant

class NordigenEndUserAgreementsV2Test: BaseNordigenTest() {

    private val dummyAgreements = listOf(EndUserAgreement(
        id = "agreement-1",
        institutionId = "OPEN_BANKING_TEST_BANK",
        created = Instant.parse("2021-11-29T18:43:35Z"),
        maxHistoricalDays = 180,
        accessValidForDays = 365,
        accessScope = listOf(BALANCES, TRANSACTIONS)
    ), EndUserAgreement(
        id = "agreement-2",
        institutionId = "OPEN_BANKING_TEST_BANK",
        created = Instant.parse("2021-11-29T18:43:35Z"),
        accepted = Instant.parse("2021-12-29T10:00:00Z"),
        maxHistoricalDays = 123,
        accessValidForDays = 456,
        accessScope = listOf(BALANCES, TRANSACTIONS, DETAILS)
    ))

    private fun mockDummyAgreementResponse() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "count" to 10,
                "results" to listOf(mapOf(
                    "id" to "agreement-1",
                    "institution_id" to "OPEN_BANKING_TEST_BANK",
                    "created" to "2021-11-29T18:43:35Z",
                    "accepted" to null,
                    "max_historical_days" to 180,
                    "access_valid_for_days" to 365,
                    "access_scope" to listOf("balances", "transactions")
                ), mapOf(
                    "id" to "agreement-2",
                    "institution_id" to "OPEN_BANKING_TEST_BANK",
                    "created" to "2021-11-29T18:43:35Z",
                    "accepted" to "2021-12-29T10:00:00Z",
                    "max_historical_days" to 123,
                    "access_valid_for_days" to 456,
                    "access_scope" to listOf("balances", "transactions", "details")
                ))
            ))
        }
    }

    @Test
    fun `NordigenKlient is able to list all agreements for a given page`() {
        mockDummyAgreementResponse()

        val agreements = mockedNordigenKlient().v2.endUserAgreements.getAll()

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/agreements/enduser/")
        }

        assertThat(agreements.count).isEqualTo(10)
        assertThat(agreements.currentPage).hasSize(2)
        assertThat(agreements.currentPage).containsExactlyInAnyOrder(dummyAgreements[0], dummyAgreements[1])
    }

    @Test
    fun `NordigenKlient is able to list all agreements for a given page using limit and offset`() {
        mockDummyAgreementResponse()

        val agreements = mockedNordigenKlient().v2.endUserAgreements.getAll(limit = 10, offset = 5)

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).startsWith("${endpoint}/agreements/enduser/?")
            assertThat(queryParams).hasSize(2)
            assertThat(queryParams.getValue("limit").values()).containsOnly("10")
            assertThat(queryParams.getValue("offset").values()).containsOnly("5")
        }

        assertThat(agreements.count).isEqualTo(10)
        assertThat(agreements.currentPage).hasSize(2)
        assertThat(agreements.currentPage).containsExactlyInAnyOrder(dummyAgreements[0], dummyAgreements[1])
    }

    @Test
    fun `NordigenKlient is able to return the details of an agreement`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "id" to "agreement-1",
                "institution_id" to "OPEN_BANKING_TEST_BANK",
                "created" to "2021-11-29T18:43:35Z",
                "accepted" to null,
                "max_historical_days" to 180,
                "access_valid_for_days" to 365,
                "access_scope" to listOf("balances", "transactions")
            ))
        }

        val agreement = mockedNordigenKlient().v2.endUserAgreements.get("req-agreement-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/agreements/enduser/req-agreement-id/")
        }

        assertThat(agreement).isEqualTo(dummyAgreements[0])
    }

    @Test
    fun `NordigenKlient is able to delete an agreement`() {
        mockHttpResponse {
            statusCode = 200
        }

        mockedNordigenKlient().v2.endUserAgreements.delete("req-agreement-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(DELETE)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/agreements/enduser/req-agreement-id/")
        }
    }

    @Test
    fun `NordigenKlient is able to accept an agreement`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "user_agent" to "user-agent",
                "ip_address" to "127.0.0.1",
            ))
        }

        val agreement = mockedNordigenKlient().v2.endUserAgreements.accept("req-agreement-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(PUT)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/agreements/enduser/req-agreement-id/accept/")
        }

        assertThat(agreement).isEqualTo(EndUserAgreementAcceptanceInfo(
            userAgent = "user-agent",
            ipAddress = "127.0.0.1"
        ))
    }

    @Test
    fun `NordigenKlient is able to create a new agreement`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "id" to "agreement-1",
                "institution_id" to "OPEN_BANKING_TEST_BANK",
                "created" to "2021-11-29T18:43:35Z",
                "accepted" to null,
                "max_historical_days" to 180,
                "access_valid_for_days" to 365,
                "access_scope" to listOf("balances", "transactions")
            ))
        }

        val agreement = mockedNordigenKlient().v2.endUserAgreements.create(
            institutionId = "OPEN_BANKING_TEST_BANK",
            maxHistoricalDays = 123,
            accessValidForDays = 365,
            accessScopes = listOf(BALANCES, TRANSACTIONS, DETAILS)
        )

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(POST)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/agreements/enduser/")
            fromJsonObjectString(bodyAsString).apply {
                assertThat(this!!.keys).containsExactlyInAnyOrder(
                    "institution_id",
                    "max_historical_days",
                    "access_valid_for_days",
                    "access_scope")

                assertThat(get("institution_id")).isEqualTo("OPEN_BANKING_TEST_BANK")
                assertThat(get("max_historical_days")).isEqualTo("123")
                assertThat(get("access_valid_for_days")).isEqualTo("365")
                assertThat(get("access_scope")).isEqualTo(listOf(
                    "balances",
                    "transactions",
                    "details"
                ))
            }
        }

        assertThat(agreement).isEqualTo(dummyAgreements[0])
    }

    @Test
    fun `NordigenKlient handles HTTP errors for all end user agreement requests`() {
        val requestFunctions = listOf(
            { mockedNordigenKlient().v2.endUserAgreements.getAll() },
            { mockedNordigenKlient().v2.endUserAgreements.get("agree-id") },
            { mockedNordigenKlient().v2.endUserAgreements.delete("agree-id") },
            { mockedNordigenKlient().v2.endUserAgreements.accept("agree-id") },
            { mockedNordigenKlient().v2.endUserAgreements.create(
                institutionId = "OPEN_BANKING_TEST_BANK",
                maxHistoricalDays = 123,
                accessValidForDays = 365,
                accessScopes = listOf(BALANCES, TRANSACTIONS, DETAILS)
            ) },
        )

        assertNordigenApiExceptionIsThrownWithPartialInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithFullInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithNoInfo(requestFunctions)
    }
}