package com.gitlab.jdiasamaro.nordigen

import assertk.assertThat
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import assertk.assertions.isFailure
import assertk.assertions.isNull
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.anyRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.anyUrl
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder
import com.github.tomakehurst.wiremock.verification.LoggedRequest
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.ACCEPT
import com.gitlab.jdiasamaro.konnect.utils.HttpHeader.AUTHORIZATION
import com.marcinziolo.kotlin.wiremock.SpecifyResponse
import com.marcinziolo.kotlin.wiremock.delete
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.like
import com.marcinziolo.kotlin.wiremock.post
import com.marcinziolo.kotlin.wiremock.put
import com.marcinziolo.kotlin.wiremock.returns
import com.squareup.moshi.Moshi
import org.junit.After
import org.junit.Before
import java.net.ServerSocket

open class BaseNordigenTest {

    private val testPort = ServerSocket(0).use {
        it.localPort
    }

    private val wiremock = WireMockServer(
        WireMockConfiguration.options()
        .notifier(ConsoleNotifier(true))
        .port(testPort))

    private val json = Moshi.Builder().build()

    internal fun toJsonObjectString(map: Map<String, Any?>): String {
        return json.adapter<Map<String, Any?>>(Map::class.java).toJson(map)
    }

    internal fun toJsonArrayString(list: List<Any?>): String {
        return json.adapter<List<Any?>>(List::class.java).toJson(list)
    }

    internal fun fromJsonObjectString(jsonValue: String): Map<String, Any?>? {
        return json.adapter<Map<String, Any?>>(Map::class.java).fromJson(jsonValue)
    }

    internal val endpoint = "http://localhost:${testPort}"
    private val accessToken = "dummy-token"

    internal fun mockedNordigenKlient() = NordigenKlient {
        endpoint = this@BaseNordigenTest.endpoint
        authenticationProvider = onAccessTokenRequested {
            accessToken
        }
    }

    @Before
    fun setup() {
        wiremock.start()
    }

    @After
    fun tearDown() {
        wiremock.resetAll()
        wiremock.stop()
    }

    internal fun mockHttpResponse(mock: SpecifyResponse) {
        wiremock.get {
            url like "/.*"
        } returns mock

        wiremock.post {
            url like "/.*"
        } returns mock

        wiremock.put {
            url like "/.*"
        } returns mock

        wiremock.delete {
            url like "/.*"
        } returns mock
    }

    internal fun verify(patternBuilder: RequestPatternBuilder, block: LoggedRequest.() -> Unit) {
        val request = wiremock.findAll(patternBuilder).last()
        block.invoke(request)
    }

    internal fun verifyNordigenApiCall(block: LoggedRequest.() -> Unit) {
        wiremock.findAll(anyRequestedFor(anyUrl())).last().apply {
            assertThat(headers.getHeader(ACCEPT).values()).containsOnly("application/json")
            assertThat(headers.getHeader(AUTHORIZATION).values()).containsOnly("Bearer $accessToken")
            block.invoke(this)
        }
    }

    internal fun assertNordigenApiExceptionIsThrownWithFullInfo(requestFunctionsToCall: List<() -> Any>) {
        mockHttpResponse {
            statusCode = 404
            body = toJsonObjectString(mapOf(
                "summary" to "An error occurred",
                "detail" to "It was not possible to fulfil the request",
                "type" to "NotFoundError",
                "iban" to "PT5002738241719298"
            ))
        }

        requestFunctionsToCall.forEach {
            val result = assertThat {
                it.invoke()
            }

            result.isFailure().given {
                val nordigenException = it as NordigenApiException
                assertThat(nordigenException.cause).isNull()
                assertThat(nordigenException.statusCode).isEqualTo(404)
                assertThat(nordigenException.type).isEqualTo("NotFoundError")
                assertThat(nordigenException.summary).isEqualTo("An error occurred")
                assertThat(nordigenException.detail).isEqualTo("It was not possible to fulfil the request")
                assertThat(nordigenException.message).isEqualTo("[status 404, NotFoundError] An error occurred - It was not possible to fulfil the request")
                assertThat(nordigenException.rawInfo).isEqualTo(mapOf(
                    "detail" to "It was not possible to fulfil the request",
                    "summary" to "An error occurred",
                    "iban" to "PT5002738241719298",
                    "type" to "NotFoundError",
                ))
            }

            wiremock.resetRequests()
        }
    }

    internal fun assertNordigenApiExceptionIsThrownWithNoInfo(requestFunctionsToCall: List<() -> Any>) {
        mockHttpResponse {
            statusCode = 503
            body = toJsonObjectString(mapOf(
                "summary" to "",
                "type" to ""
            ))
        }

        requestFunctionsToCall.forEach {
            val result = assertThat {
                it.invoke()
            }

            result.isFailure().given {
                val nordigenException = it as NordigenApiException
                assertThat(nordigenException.cause).isNull()
                assertThat(nordigenException.statusCode).isEqualTo(503)
                assertThat(nordigenException.type).isNull()
                assertThat(nordigenException.summary).isNull()
                assertThat(nordigenException.detail).isNull()
                assertThat(nordigenException.message).isEqualTo("[status 503] ")
                assertThat(nordigenException.rawInfo).isEqualTo(mapOf(
                    "summary" to "",
                    "type" to ""
                ))
            }

            wiremock.resetRequests()
        }
    }

    internal fun assertNordigenApiExceptionIsThrownWithPartialInfo(requestFunctionsToCall: List<() -> Any>) {
        mockHttpResponse {
            statusCode = 429
            body = toJsonObjectString(mapOf(
                "summary" to "",
                "detail" to "It was not possible to fulfil the request",
                "iban" to "PT5002738241719298"
            ))
        }

        requestFunctionsToCall.forEach {
            val result = assertThat {
                it.invoke()
            }

            result.isFailure().given {
                val nordigenException = it as NordigenApiException
                assertThat(nordigenException.cause).isNull()
                assertThat(nordigenException.statusCode).isEqualTo(429)
                assertThat(nordigenException.type).isNull()
                assertThat(nordigenException.summary).isNull()
                assertThat(nordigenException.detail).isEqualTo("It was not possible to fulfil the request")
                assertThat(nordigenException.message).isEqualTo("[status 429] It was not possible to fulfil the request")
                assertThat(nordigenException.rawInfo).isEqualTo(mapOf(
                    "detail" to "It was not possible to fulfil the request",
                    "summary" to "",
                    "iban" to "PT5002738241719298"
                ))
            }

            wiremock.resetRequests()
        }
    }
}