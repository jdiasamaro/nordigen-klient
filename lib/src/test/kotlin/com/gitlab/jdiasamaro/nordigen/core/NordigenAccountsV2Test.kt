package com.gitlab.jdiasamaro.nordigen.core

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.containsOnly
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.github.tomakehurst.wiremock.http.RequestMethod.GET
import com.gitlab.jdiasamaro.nordigen.BaseNordigenTest
import com.gitlab.jdiasamaro.nordigen.model.AccountBalance
import com.gitlab.jdiasamaro.nordigen.model.AccountDetails
import com.gitlab.jdiasamaro.nordigen.model.AccountDetailsStatus.BLOCKED
import com.gitlab.jdiasamaro.nordigen.model.AccountMetadata
import com.gitlab.jdiasamaro.nordigen.model.AccountStatus.PROCESSING
import com.gitlab.jdiasamaro.nordigen.model.AccountUsage.PRIV
import com.gitlab.jdiasamaro.nordigen.model.AmountDetails
import com.gitlab.jdiasamaro.nordigen.model.BalanceType.AUTHORISED
import com.gitlab.jdiasamaro.nordigen.model.BalanceType.CLOSING_BOOKED
import com.gitlab.jdiasamaro.nordigen.model.BalanceType.EXPECTED
import com.gitlab.jdiasamaro.nordigen.model.CashAccountType.SLRY
import com.gitlab.jdiasamaro.nordigen.model.Transaction
import org.junit.Test
import java.time.Instant
import java.time.LocalDate

class NordigenAccountsV2Test: BaseNordigenTest() {

    @Test
    fun `NordigenKlient is able to return account metadata based on its ID`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "id" to "acc-1",
                "created" to "2021-11-29T18:43:35Z",
                "last_accessed" to "2021-11-29T19:43:35Z",
                "iban" to "PT5043242390482130481",
                "institution_id" to "OPEN_BANK_ID",
                "status" to "PROCESSING"
            ))
        }

        val account = mockedNordigenKlient().v2.accounts.get("acc-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/accounts/acc-id/")
        }

        assertThat(account).isEqualTo(AccountMetadata(
            id = "acc-1",
            iban = "PT5043242390482130481",
            status = PROCESSING,
            created = Instant.parse("2021-11-29T18:43:35Z"),
            lastAccessed = Instant.parse("2021-11-29T19:43:35Z"),
            institutionId = "OPEN_BANK_ID"
        ))
    }

    @Test
    fun `NordigenKlient is able to return all account details based on its ID`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "bban" to "9248901480918284102",
                "bic" to "XXYYZZ",
                "cashAccountType" to "SLRY",
                "currency" to "EUR",
                "details" to "some details",
                "displayName" to "Some Name",
                "iban" to "PT509248901480918284102",
                "linkedAccounts" to "acc-1,acc-2",
                "msisdn" to "12981098901",
                "name" to "Name",
                "ownerAddressUnstructured" to "av louise",
                "ownerName" to "I am the owner",
                "product" to "prd",
                "resourceId" to "id",
                "status" to "blocked",
                "usage" to "PRIV"
            ))
        }

        val details = mockedNordigenKlient().v2.accounts.getDetails("acc-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/accounts/acc-id/details/")
        }

        assertThat(details).isEqualTo(AccountDetails(
            bban = "9248901480918284102",
            bic = "XXYYZZ",
            cashAccountType = SLRY,
            currency = "EUR",
            details = "some details",
            displayName = "Some Name",
            iban = "PT509248901480918284102",
            linkedAccounts = "acc-1,acc-2",
            msisdn = "12981098901",
            name = "Name",
            ownerAddressUnstructured = "av louise",
            ownerName = "I am the owner",
            product = "prd",
            resourceId = "id",
            status = BLOCKED,
            usage = PRIV
        ))
    }

    @Test
    fun `NordigenKlient is able to return all balance types associated with an account`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "balances" to listOf(mapOf(
                    "balanceAmount" to mapOf(
                        "amount" to "+300.45",
                        "currency" to "EUR"
                    ),
                    "balanceType" to "authorised",
                    "creditLimitIncluded" to true,
                    "referenceDate" to "2021-11-30"
                ), mapOf(
                    "balanceAmount" to mapOf(
                        "amount" to "+900.00",
                        "currency" to "EUR"
                    ),
                    "balanceType" to "closingBooked",
                    "lastChangeDateTime" to "2021-11-29T18:43:35Z"
                ), mapOf(
                    "balanceAmount" to mapOf(
                        "amount" to "-50.23",
                        "currency" to "EUR"
                    ),
                    "balanceType" to "expected",
                    "lastCommittedTransaction" to "id"
                ))
            ))
        }

        val balances = mockedNordigenKlient().v2.accounts.getBalances("acc-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/accounts/acc-id/balances/")
        }

        assertThat(balances).hasSize(3)
        assertThat(balances).containsExactlyInAnyOrder(AccountBalance(
            balanceAmount = AmountDetails(
                amount = 300.45,
                currency = "EUR"
            ),
            balanceType = AUTHORISED,
            creditLimitIncluded = true,
            referenceDate = LocalDate.of(2021, 11, 30)
        ), AccountBalance(
            balanceAmount = AmountDetails(
                amount = 900.00,
                currency = "EUR"
            ),
            balanceType = CLOSING_BOOKED,
            lastChangeDateTime = Instant.parse("2021-11-29T18:43:35Z")
        ), AccountBalance(
            balanceAmount = AmountDetails(
                amount = -50.23,
                currency = "EUR"
            ),
            balanceType = EXPECTED,
            lastCommittedTransaction = "id"
        ))
    }

    @Test
    fun `NordigenKlient is able to list all transactions for an account`() {
        mockHttpResponse {
            statusCode = 200
            body = toJsonObjectString(mapOf(
                "transactions" to mapOf(
                    "booked" to listOf(mapOf(
                        "transactionAmount" to mapOf(
                            "amount" to "-20.45",
                            "currency" to "EUR"
                        ),
                        "additionalInformation" to "some info",
                        "bankTransactionCode" to "code",
                        "checkId" to "qwerty",
                        "creditorAgent" to "c-agent",
                        "creditorId" to "c-id",
                        "creditorName" to "c-name",
                        "debtorAgent" to "d-agent",
                        "debtorName" to "d-name",
                        "entryReference" to "entry",
                        "mandateId" to "id",
                        "proprietaryBankTransactionCode" to "code",
                        "remittanceInformationStructured" to "info",
                        "remittanceInformationUnstructured" to "description",
                        "transactionId" to "1312235",
                        "ultimateCreditor" to "uc",
                        "ultimateDebtor" to "ud",
                        "bookingDate" to "2021-10-10",
                        "valueDate" to "2021-10-11"
                    )),
                    "pending" to listOf(mapOf(
                        "transactionAmount" to mapOf(
                            "amount" to "100.27",
                            "currency" to "EUR"
                        ),
                        "bookingDate" to "2021-10-10",
                        "valueDate" to "2021-10-11",
                        "remittanceInformationUnstructured" to "transaction description",
                        "transactionId" to "19"
                    ), mapOf(
                        "transactionAmount" to mapOf(
                            "amount" to "-20.30",
                            "currency" to "EUR"
                        ),
                        "bookingDate" to "2021-10-29",
                        "valueDate" to "2021-10-30",
                        "remittanceInformationUnstructured" to "some tx",
                        "transactionId" to "20"
                    ))
                )
            ))
        }

        val transactions = mockedNordigenKlient().v2.accounts.getTransactions("acc-id")

        verifyNordigenApiCall {
            assertThat(method).isEqualTo(GET)
            assertThat(absoluteUrl).isEqualTo("${endpoint}/accounts/acc-id/transactions/")
        }

        assertThat(transactions.booked).hasSize(1)
        assertThat(transactions.pending).hasSize(2)

        assertThat(transactions.booked).containsOnly(Transaction(
            transactionAmount = AmountDetails(-20.45, "EUR"),
            additionalInformation = "some info",
            bankTransactionCode = "code",
            checkId = "qwerty",
            creditorAgent = "c-agent",
            creditorId = "c-id",
            creditorName = "c-name",
            debtorAgent = "d-agent",
            debtorName = "d-name",
            entryReference = "entry",
            mandateId = "id",
            proprietaryBankTransactionCode = "code",
            remittanceInformationStructured = "info",
            remittanceInformationUnstructured = "description",
            transactionId = "1312235",
            ultimateCreditor = "uc",
            ultimateDebtor = "ud",
            bookingDate = LocalDate.parse("2021-10-10"),
            valueDate = LocalDate.parse("2021-10-11")
        ))

        assertThat(transactions.pending).containsExactlyInAnyOrder(Transaction(
            transactionAmount = AmountDetails(100.27, "EUR"),
            bookingDate = LocalDate.parse("2021-10-10"),
            valueDate = LocalDate.parse("2021-10-11"),
            remittanceInformationUnstructured = "transaction description",
            transactionId = "19"
        ), Transaction(
            transactionAmount = AmountDetails(-20.30, "EUR"),
            bookingDate = LocalDate.parse("2021-10-29"),
            valueDate = LocalDate.parse("2021-10-30"),
            remittanceInformationUnstructured = "some tx",
            transactionId = "20"
        ))
    }

    @Test
    fun `NordigenKlient handles HTTP errors for all account requests`() {
        val requestFunctions = listOf(
            { mockedNordigenKlient().v2.accounts.get("acc-id") },
            { mockedNordigenKlient().v2.accounts.getDetails("acc-id") },
            { mockedNordigenKlient().v2.accounts.getBalances("acc-id") },
            { mockedNordigenKlient().v2.accounts.getTransactions("acc-id") }
        )

        assertNordigenApiExceptionIsThrownWithPartialInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithFullInfo(requestFunctions)
        assertNordigenApiExceptionIsThrownWithNoInfo(requestFunctions)
    }
}