package com.gitlab.jdiasamaro.nordigen.core

import com.gitlab.jdiasamaro.nordigen.NordigenAuthenticationProvider
import com.gitlab.jdiasamaro.nordigen.NordigenKlient
import com.gitlab.jdiasamaro.nordigen.model.NordigenAccessToken
import java.time.Instant

internal class DefaultAuthenticationProvider(private val nordigen: NordigenKlient): NordigenAuthenticationProvider {

    private val tokenLock = Any()
    private lateinit var token: AccessTokenInfo

    private val secretId = nordigen.configs.secretId ?: throw IllegalStateException("Secret ID must be provided")
    private val secretKey = nordigen.configs.secretKey ?: throw IllegalStateException("Secret key must be provided")

    override fun getAccessToken(): String {
        return synchronized(tokenLock) {
            val now = Instant.now()

            if (!this::token.isInitialized || token.refreshTokenValidUntil < now) {
                fetchNewAccessToken(now)
            }

            if (token.accessTokenValidUntil < now) {
                refreshAccessToken(now)
            }

            token.info.accessToken
        }
    }

    private fun fetchNewAccessToken(now: Instant) {
        val newToken = nordigen.v2.tokens.create(secretId, secretKey)

        token = AccessTokenInfo(
            info = newToken,
            accessTokenValidUntil = now.plusSeconds(newToken.accessTokenExpireSeconds),
            refreshTokenValidUntil = now.plusSeconds(newToken.refreshTokenExpireSeconds)
        )
    }

    private fun refreshAccessToken(now: Instant) {
        val refreshedToken = nordigen.v2.tokens.refresh(token.info.refreshToken)

        token = token.copy(
            info = token.info.copy(
                accessToken = refreshedToken.accessToken,
                accessTokenExpireSeconds = refreshedToken.accessTokenExpireSeconds
            ),
            accessTokenValidUntil = now.plusSeconds(refreshedToken.accessTokenExpireSeconds)
        )
    }
}

private data class AccessTokenInfo(
    val info: NordigenAccessToken,
    val accessTokenValidUntil: Instant,
    val refreshTokenValidUntil: Instant
)
