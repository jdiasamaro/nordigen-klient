package com.gitlab.jdiasamaro.nordigen.model

import com.squareup.moshi.Json
import java.time.Instant

data class Requisition(
    val id: String,
    val reference: String? = null,
    val ssn: String? = null,

    val status: RequisitionStatus,

    @Json(name = "account_selection")
    val accountSelection: Boolean,

    @Json(name = "user_language")
    val userLanguage: String? = null,

    @Json(name = "link")
    val oauthAuthorizationUrl: String,

    @Json(name = "accounts")
    val accountIds: List<String>,

    @Json(name = "institution_id")
    val institutionId: String,

    @Json(name = "redirect")
    val redirectUrl: String? = null,

    val created: Instant? = null,

    @Json(name = "agreement")
    val endUserAgreementId: String? = null,
)

data class RequisitionStatusInfo(
    val short: String,
    val long: RequisitionStatus,
    val description: String
)

enum class RequisitionStatus(val short: String) {
    CREATED("CR"),
    LINKED("LN"),
    EXPIRED("EX"),
    REJECTED("RJ"),
    UNDERGOING_AUTHENTICATION("UA"),
    GRANTING_ACCESS("GA"),
    SELECTING_ACCOUNTS("SA"),
    GIVING_CONSENT("GC");

    companion object {
        fun fromShortName(short: String): RequisitionStatus {
            return values().first {
                it.short.equals(short, ignoreCase = true)
            }
        }
    }
}

data class PaginatedRequisitionList(
    val count: Int,

    @Json(name = "results")
    val currentPage: List<Requisition>
)