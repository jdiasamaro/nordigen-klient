package com.gitlab.jdiasamaro.nordigen

import java.util.function.Supplier

/**
 * Used to provide a valid access token for the API client. Implementations are expected to
 * be able to renew this token when necessary. Otherwise, API calls might fail with 401 UNAUTHORIZED.
 */
interface NordigenAuthenticationProvider {
    fun getAccessToken(): String
}

/**
 * Simple implementation of a NordigenAuthenticationProvider. The token supplier will be executed on every API request.
 */
fun onAccessTokenRequested(tokenProvider: Supplier<String>) = object: NordigenAuthenticationProvider {
    override fun getAccessToken(): String {
        return tokenProvider.get()
    }
}