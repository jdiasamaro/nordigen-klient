package com.gitlab.jdiasamaro.nordigen.model

import com.squareup.moshi.Json

data class NordigenAccessToken(
    @Json(name = "access")
    val accessToken: String,

    @Json(name = "access_expires")
    val accessTokenExpireSeconds: Long,

    @Json(name = "refresh")
    val refreshToken: String,

    @Json(name = "refresh_expires")
    val refreshTokenExpireSeconds: Long
)

data class RefreshedNordigenAccessToken(
    @Json(name = "access")
    val accessToken: String,

    @Json(name = "access_expires")
    val accessTokenExpireSeconds: Long
)