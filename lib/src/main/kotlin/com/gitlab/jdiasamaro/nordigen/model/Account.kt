package com.gitlab.jdiasamaro.nordigen.model

import com.squareup.moshi.Json
import java.time.Instant

data class AccountMetadata(
    val id: String,
    val iban: String,
    val status: AccountStatus,

    val created: Instant,

    @Json(name = "last_accessed")
    val lastAccessed: Instant? = null,

    @Json(name = "institution_id")
    val institutionId: String
)

/**
 * Complete details on all optional and mandatory fields: https://nordigen.com/en/docs/account-information/output/accounts/
 */
data class AccountDetails(
    val bban: String? = null,
    val bic: String? = null,
    val cashAccountType: CashAccountType? = null,
    val currency: String,
    val details: String? = null,
    val displayName: String? = null,
    val iban: String? = null,
    val linkedAccounts: String? = null,
    val msisdn: String? = null,
    val name: String? = null,
    val ownerAddressUnstructured: String? = null,
    val ownerName: String? = null,
    val product: String? = null,
    val resourceId: String? = null,
    val usage: AccountUsage? = null,
    val status: AccountDetailsStatus? = null
)


/**
 * Possible ISO 20022 values: https://open-banking.pass-consulting.com/json_ExternalCashAccountType1Code.html
 */
enum class CashAccountType {
    CACC, CASH, CHAR, CISH, COMM, CPAC, LLSV, LOAN, MGLD, MOMA, NREX, ODFT, ONDP, OTHR, SACC, SLRY, SVGS, TAXE, TRAN, TRAS
}

enum class AccountDetailsStatus {
    ENABLED, DELETED, BLOCKED
}

enum class AccountUsage {
    PRIV, ORGA
}

enum class AccountStatus {
    DISCOVERED,
    PROCESSING,
    ERROR,
    EXPIRED,
    READY,
    SUSPENDED
}