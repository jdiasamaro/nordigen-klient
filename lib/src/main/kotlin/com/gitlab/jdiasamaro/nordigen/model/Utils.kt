package com.gitlab.jdiasamaro.nordigen.model

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonReader.Token.BEGIN_OBJECT
import com.squareup.moshi.JsonReader.Token.END_OBJECT
import com.squareup.moshi.JsonReader.Token.NULL
import com.squareup.moshi.JsonReader.Token.STRING
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson

internal class EndUserAgreementAccessScopeJsonAdapter: JsonAdapter<EndUserAgreementAccessScope>() {

    @FromJson
    override fun fromJson(reader: JsonReader): EndUserAgreementAccessScope? {
        return when (reader.peek()) {
            NULL -> reader.nextNull()
            STRING -> EndUserAgreementAccessScope.valueOf(reader.nextString().uppercase())
            else -> throw JsonDataException("Invalid EndUserAgreementAccessScope type (${reader.peek()}) at path ${reader.path}")
        }
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: EndUserAgreementAccessScope?) {
        if (value == null) {
            writer.nullValue()
        } else {
            writer.value(value.name.lowercase())
        }
    }
}

internal class RequisitionStatusJsonAdapter: JsonAdapter<RequisitionStatus>() {

    @FromJson
    override fun fromJson(reader: JsonReader): RequisitionStatus? {
        return when (reader.peek()) {
            NULL -> reader.nextNull()
            STRING -> RequisitionStatus.fromShortName(reader.nextString())
            BEGIN_OBJECT -> {
                reader.beginObject()
                var statusValue: String? = null
                while (reader.peek() != END_OBJECT) {
                    if (reader.nextName() == "short") {
                        statusValue = reader.nextString()
                    } else {
                        reader.skipValue()
                    }
                }
                reader.endObject()

                statusValue?.let {
                    RequisitionStatus.fromShortName(it)
                } ?: throw JsonDataException("Can't find requisition status at ${reader.path}")
            }
            else -> throw JsonDataException("Invalid RequisitionStatus type (${reader.peek()}) at path ${reader.path}")
        }
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: RequisitionStatus?) {
        if (value == null) {
            writer.nullValue()
        } else {
            writer.value(value.name.lowercase())
        }
    }
}

internal class AccountDetailsStatusJsonAdapter: JsonAdapter<AccountDetailsStatus>() {

    @FromJson
    override fun fromJson(reader: JsonReader): AccountDetailsStatus? {
        return when (reader.peek()) {
            NULL -> reader.nextNull()
            STRING -> AccountDetailsStatus.valueOf(reader.nextString().uppercase())
            else -> throw JsonDataException("Invalid AccountDetailsStatus type (${reader.peek()}) at path ${reader.path}")
        }
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: AccountDetailsStatus?) {
        if (value == null) {
            writer.nullValue()
        } else {
            writer.value(value.name.lowercase())
        }
    }
}
internal class BalanceTypeJsonAdapter: JsonAdapter<BalanceType>() {

    @FromJson
    override fun fromJson(reader: JsonReader): BalanceType? {
        return when (reader.peek()) {
            NULL -> reader.nextNull()
            STRING -> BalanceType.fromValue(reader.nextString())
            else -> throw JsonDataException("Invalid BalanceType type (${reader.peek()}) at path ${reader.path}")
        }
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: BalanceType?) {
        if (value == null) {
            writer.nullValue()
        } else {
            writer.value(value.name.lowercase())
        }
    }
}