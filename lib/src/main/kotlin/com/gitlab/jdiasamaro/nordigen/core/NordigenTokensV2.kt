package com.gitlab.jdiasamaro.nordigen.core

import com.gitlab.jdiasamaro.nordigen.NordigenKlient
import com.gitlab.jdiasamaro.nordigen.NordigenV2
import com.gitlab.jdiasamaro.nordigen.model.NordigenAccessToken
import com.gitlab.jdiasamaro.nordigen.model.RefreshedNordigenAccessToken
import com.gitlab.jdiasamaro.nordigen.nordigenApiException

internal class NordigenTokensV2(private val nordigen: NordigenKlient): NordigenV2.Tokens {

    override fun create(secretId: String, secretKey: String): NordigenAccessToken {
        val secretInfo = mapOf(
            "secret_id" to secretId,
            "secret_key" to secretKey
        )

        return nordigen.httpClient.post("/token/new/", secretInfo).use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun refresh(refreshToken: String): RefreshedNordigenAccessToken {
        val refreshInfo = mapOf("refresh" to refreshToken)

        return nordigen.httpClient.post("/token/refresh/", refreshInfo).use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }
}