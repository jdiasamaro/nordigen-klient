package com.gitlab.jdiasamaro.nordigen

const val DEFAULT_NORDIGEN_ENDPOINT = "https://ob.nordigen.com/api/v2"

data class NordigenConfig(
    /**
     * The default NordigenAuthenticationProvider relies on the secret ID to create access tokens. If this
     * is not supplied, the authentication provider must be replaced. Otherwise, API calls will fail.
     */
    var secretId: String? = null,

    /**
     * The default NordigenAuthenticationProvider relies on the secret key to create access tokens. If this
     * is not supplied, the authentication provider must be replaced. Otherwise, API calls will fail.
     */
    var secretKey: String? = null,

    /**
     * The base endpoint used to access Nordigen API. Can be used to mock the connection for
     * testing purposes
     */
    var endpoint: String = DEFAULT_NORDIGEN_ENDPOINT,

    /**
     * Used by the NordigenKlient instance to provide access tokens for the API calls. If not
     * defined, the default provider will be used
     */
    var authenticationProvider: NordigenAuthenticationProvider? = null
)