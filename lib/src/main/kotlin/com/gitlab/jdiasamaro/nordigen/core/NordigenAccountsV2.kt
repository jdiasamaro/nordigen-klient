package com.gitlab.jdiasamaro.nordigen.core

import com.gitlab.jdiasamaro.nordigen.NordigenKlient
import com.gitlab.jdiasamaro.nordigen.NordigenV2
import com.gitlab.jdiasamaro.nordigen.model.AccountBalance
import com.gitlab.jdiasamaro.nordigen.model.AccountBalanceList
import com.gitlab.jdiasamaro.nordigen.model.AccountDetails
import com.gitlab.jdiasamaro.nordigen.model.AccountMetadata
import com.gitlab.jdiasamaro.nordigen.model.TransactionsList
import com.gitlab.jdiasamaro.nordigen.model.TransactionsResult
import com.gitlab.jdiasamaro.nordigen.nordigenApiException

internal class NordigenAccountsV2(private val nordigen: NordigenKlient): NordigenV2.Accounts {

    override fun get(accountId: String): AccountMetadata {
        return nordigen.httpClient.get("/accounts/${accountId}/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun getDetails(accountId: String): AccountDetails {
        return nordigen.httpClient.get("/accounts/${accountId}/details/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun getBalances(accountId: String): List<AccountBalance> {
        return nordigen.httpClient.get("/accounts/${accountId}/balances/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue<AccountBalanceList>().balances
            }
        }
    }

    override fun getTransactions(accountId: String): TransactionsList {
        return nordigen.httpClient.get("/accounts/${accountId}/transactions/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue<TransactionsResult>().transactions
            }
        }
    }
}