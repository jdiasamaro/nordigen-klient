package com.gitlab.jdiasamaro.nordigen

import com.gitlab.jdiasamaro.konnect.core.response.HttpResponse

/**
 * Contains all information about an HTTP error returned by the Nordigen API.
 *
 * The rawInfo field contains the original response value object
 */
class NordigenApiException(
    val statusCode: Int,
    val type: String? = null,
    val detail: String? = null,
    val summary: String? = null,
    val rawInfo: Map<String, Any?>? = null,
    override val message: String? = null,
    override val cause: Throwable? = null
): Exception()

fun nordigenApiException(response: HttpResponse): NordigenApiException {
    val errorInfo = response.toJsonObject()

    val type = errorInfo["type"]?.toString()?.ifBlank { null }
    val detail = errorInfo["detail"]?.toString()?.ifBlank { null }
    val summary = errorInfo["summary"]?.toString()?.ifBlank { null }
    val message = "[status ${listOfNotNull(response.status, type).joinToString(", ")}] ${listOfNotNull(summary, detail).joinToString(" - ")}"

    return NordigenApiException(
        type = type,
        detail = detail,
        message = message,
        summary = summary,
        rawInfo = errorInfo,
        statusCode = response.status,
    )
}