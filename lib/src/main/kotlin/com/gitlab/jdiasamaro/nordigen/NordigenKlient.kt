package com.gitlab.jdiasamaro.nordigen

import com.gitlab.jdiasamaro.konnect.Konnect
import com.gitlab.jdiasamaro.nordigen.core.DefaultAuthenticationProvider
import com.gitlab.jdiasamaro.nordigen.core.NordigenAccountsV2
import com.gitlab.jdiasamaro.nordigen.core.NordigenEndUserAgreementsV2
import com.gitlab.jdiasamaro.nordigen.core.NordigenInstitutionsV2
import com.gitlab.jdiasamaro.nordigen.core.NordigenRequisitionsV2
import com.gitlab.jdiasamaro.nordigen.core.NordigenTokensV2
import com.gitlab.jdiasamaro.nordigen.model.AccountDetailsStatus
import com.gitlab.jdiasamaro.nordigen.model.AccountDetailsStatusJsonAdapter
import com.gitlab.jdiasamaro.nordigen.model.BalanceType
import com.gitlab.jdiasamaro.nordigen.model.BalanceTypeJsonAdapter
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScope
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScopeJsonAdapter
import com.gitlab.jdiasamaro.nordigen.model.RequisitionStatus
import com.gitlab.jdiasamaro.nordigen.model.RequisitionStatusJsonAdapter
import com.squareup.moshi.JsonAdapter

class NordigenKlient(configBuilder: (NordigenConfig.() -> Unit)? = null) {

    val configs = NordigenConfig().apply {
        configBuilder?.invoke(this)
    }

    internal val httpClient = Konnect {
        baseUrl = configs.endpoint
        defaults {
            headers {
                accept set "application/json"
            }
        }

        jsonAdapters {
            add(JsonAdapter.Factory { type, _, _ ->
                when (type) {
                    EndUserAgreementAccessScope::class.java -> EndUserAgreementAccessScopeJsonAdapter()
                    AccountDetailsStatus::class.java -> AccountDetailsStatusJsonAdapter()
                    RequisitionStatus::class.java -> RequisitionStatusJsonAdapter()
                    BalanceType::class.java -> BalanceTypeJsonAdapter()
                    else -> null
                }
            })
        }
    }

    internal val authentication = configs.authenticationProvider ?: DefaultAuthenticationProvider(this)

    val v2 = object: NordigenV2 {
        override val tokens = NordigenTokensV2(this@NordigenKlient)
        override val institutions = NordigenInstitutionsV2(this@NordigenKlient)
        override val endUserAgreements = NordigenEndUserAgreementsV2(this@NordigenKlient)
        override val requisitions = NordigenRequisitionsV2(this@NordigenKlient)
        override val accounts = NordigenAccountsV2(this@NordigenKlient)
    }
}