package com.gitlab.jdiasamaro.nordigen.core

import com.gitlab.jdiasamaro.nordigen.NordigenKlient
import com.gitlab.jdiasamaro.nordigen.NordigenV2
import com.gitlab.jdiasamaro.nordigen.model.PaginatedRequisitionList
import com.gitlab.jdiasamaro.nordigen.model.Requisition
import com.gitlab.jdiasamaro.nordigen.nordigenApiException

internal class NordigenRequisitionsV2(private val nordigen: NordigenKlient): NordigenV2.Requisitions {

    override fun getAll(limit: Int?, offset: Int?): PaginatedRequisitionList {
        return nordigen.httpClient.get("/requisitions/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }

            queryParameters {
                limit?.let { param("limit") set it }
                offset?.let { param("offset") set it }
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun get(requisitionId: String): Requisition {
        return nordigen.httpClient.get("/requisitions/${requisitionId}/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun delete(requisitionId: String) {
        nordigen.httpClient.delete("/requisitions/${requisitionId}/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            }
        }
    }

    override fun create(institutionId: String, redirectUrl: String, reference: String?, userLanguage: String?, endUserAgreementId: String?): Requisition {
        val requestBody = mutableMapOf(
            "institution_id" to institutionId,
            "redirect" to redirectUrl
        )

        reference?.let { requestBody["reference"] = it }
        userLanguage?.let { requestBody["user_language"] = it }
        endUserAgreementId?.let { requestBody["agreement"] = it }

        return nordigen.httpClient.post("/requisitions/", requestBody) {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }
}