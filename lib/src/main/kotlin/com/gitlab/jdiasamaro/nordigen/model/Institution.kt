package com.gitlab.jdiasamaro.nordigen.model

import com.squareup.moshi.Json

data class Institution(
    val id: String,
    val name: String,
    val bic: String? = null,
    val countries: List<String>,

    @Json(name = "logo")
    val logoUrl: String,

    @Json(name = "transaction_total_days")
    val transactionTotalDays: Int? = null
)