package com.gitlab.jdiasamaro.nordigen.model

import java.time.LocalDate

/**
 * Details of optional and mandatory fields: https://nordigen.com/en/docs/account-information/output/transactions/
 *
 * Note: This model does not strictly follow the structure of the Nordigen API response. The API docs are not clear
 * on some transaction fields, namely if fields are strings or complex objects. These fields are returned on this
 * model but treated as genetic JSON elements (primitives, arrays or objects).
 *
 * As such, this model is created from each individual field and simplifies the output of the NordigenKlient.
 */
data class Transaction(
    val transactionAmount: AmountDetails,

    val additionalInformation: String? = null,
    val bankTransactionCode: String? = null,
    val checkId: String? = null,
    val creditorAgent: String? = null,
    val creditorId: String? = null,
    val creditorName: String? = null,
    val debtorAgent: String? = null,
    val debtorName: String? = null,
    val entryReference: String? = null,
    val mandateId: String? = null,
    val proprietaryBankTransactionCode: String? = null,
    val remittanceInformationStructured: String? = null,
    val remittanceInformationUnstructured: String? = null,
    val transactionId: String? = null,
    val ultimateCreditor: String? = null,
    val ultimateDebtor: String? = null,
    val bookingDate: LocalDate? = null,
    val valueDate: LocalDate? = null,
) {
    val amount = transactionAmount.amount
    val currency = transactionAmount.currency
    val description = remittanceInformationUnstructured
}

data class TransactionsResult(
    val transactions: TransactionsList
)

data class TransactionsList(
    val booked: List<Transaction>,
    val pending: List<Transaction>
)