package com.gitlab.jdiasamaro.nordigen.model

import java.time.Instant
import java.time.LocalDate

/**
 * Details of optional and mandatory fields: https://nordigen.com/en/docs/account-information/output/balance/
 */
data class AccountBalance(
    val balanceAmount: AmountDetails,
    val balanceType: BalanceType,
    val lastChangeDateTime: Instant? = null,
    val creditLimitIncluded: Boolean? = null,
    val lastCommittedTransaction: String? = null,
    val referenceDate: LocalDate? = null
)

data class AccountBalanceList(
    val balances: List<AccountBalance>
)

enum class BalanceType(val value: String) {
    AUTHORISED("authorised"),
    CLOSING_BOOKED("closingBooked"),
    EXPECTED("expected"),
    FORWARD_AVAILABLE("forwardAvailable"),
    INTERIM_AVAILABLE("interimAvailable"),
    INTERIM_BOOKED("interimBooked"),
    NON_INVOICED("nonInvoiced"),
    OPENING_BOOKED("openingBooked");

    companion object {
        fun fromValue(value: String): BalanceType {
            return values().first {
                it.value == value
            }
        }
    }
}