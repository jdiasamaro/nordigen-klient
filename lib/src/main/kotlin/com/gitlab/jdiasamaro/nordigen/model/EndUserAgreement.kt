package com.gitlab.jdiasamaro.nordigen.model

import com.squareup.moshi.Json
import java.time.Instant

data class EndUserAgreement(
    val id: String,

    @Json(name = "institution_id")
    val institutionId: String,

    val created: Instant,
    val accepted: Instant? = null,

    @Json(name = "max_historical_days")
    val maxHistoricalDays: Int,

    @Json(name = "access_valid_for_days")
    val accessValidForDays: Int,

    @Json(name = "access_scope")
    val accessScope: List<EndUserAgreementAccessScope>
)

data class PaginatedEndUserAgreementList(
    val count: Int,

    @Json(name = "results")
    val currentPage: List<EndUserAgreement>
)

data class EndUserAgreementAcceptanceInfo(
    @Json(name = "user_agent")
    val userAgent: String,

    @Json(name = "ip_address")
    val ipAddress: String
)

enum class EndUserAgreementAccessScope {
    BALANCES, DETAILS, TRANSACTIONS
}