package com.gitlab.jdiasamaro.nordigen.core

import com.gitlab.jdiasamaro.nordigen.NordigenKlient
import com.gitlab.jdiasamaro.nordigen.NordigenV2
import com.gitlab.jdiasamaro.nordigen.model.Institution
import com.gitlab.jdiasamaro.nordigen.nordigenApiException

internal class NordigenInstitutionsV2(private val nordigen: NordigenKlient): NordigenV2.Institutions {

    override fun getAll(country: String?): List<Institution> {
        return nordigen.httpClient.get("/institutions/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }

            queryParameters {
                country?.let {
                    param("country") set country
                }
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun get(institutionId: String): Institution {
        return nordigen.httpClient.get("/institutions/${institutionId}/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }
}