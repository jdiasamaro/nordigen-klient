package com.gitlab.jdiasamaro.nordigen.model

data class AmountDetails(
    val amount: Double,
    val currency: String
)