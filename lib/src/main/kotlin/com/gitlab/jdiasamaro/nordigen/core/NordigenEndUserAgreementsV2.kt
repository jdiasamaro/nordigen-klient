package com.gitlab.jdiasamaro.nordigen.core

import com.gitlab.jdiasamaro.nordigen.NordigenKlient
import com.gitlab.jdiasamaro.nordigen.NordigenV2
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreement
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAcceptanceInfo
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScope
import com.gitlab.jdiasamaro.nordigen.model.PaginatedEndUserAgreementList
import com.gitlab.jdiasamaro.nordigen.nordigenApiException

internal class NordigenEndUserAgreementsV2(private val nordigen: NordigenKlient): NordigenV2.EndUserAgreements {

    override fun getAll(limit: Int?, offset: Int?): PaginatedEndUserAgreementList {
        return nordigen.httpClient.get("/agreements/enduser/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }

            queryParameters {
                limit?.let { param("limit") set it }
                offset?.let { param("offset") set it }
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun get(agreementId: String): EndUserAgreement {
        return nordigen.httpClient.get("/agreements/enduser/${agreementId}/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun delete(agreementId: String) {
        nordigen.httpClient.delete("/agreements/enduser/${agreementId}/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            }
        }
    }

    override fun accept(agreementId: String): EndUserAgreementAcceptanceInfo {
        return nordigen.httpClient.put("/agreements/enduser/${agreementId}/accept/") {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }

    override fun create(
        institutionId: String,
        maxHistoricalDays: Int?,
        accessValidForDays: Int?,
        accessScopes: List<EndUserAgreementAccessScope>
    ): EndUserAgreement {
        val requestBody = mutableMapOf<String, Any>(
            "institution_id" to institutionId
        )

        maxHistoricalDays?.let { requestBody["max_historical_days"] = it.toString() }
        accessValidForDays?.let { requestBody["access_valid_for_days"] = it.toString() }
        if (accessScopes.isNotEmpty()) {
            requestBody["access_scope"] = accessScopes.map { it.name.lowercase() }
        }

        return nordigen.httpClient.post("/agreements/enduser/", requestBody) {
            headers {
                authorization set "Bearer ${nordigen.authentication.getAccessToken()}"
            }
        }.use {
            if (it.isClientError() || it.isServerError()) {
                throw nordigenApiException(it)
            } else {
                it.toValue()
            }
        }
    }
}