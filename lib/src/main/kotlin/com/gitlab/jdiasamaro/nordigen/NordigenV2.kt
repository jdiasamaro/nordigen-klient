package com.gitlab.jdiasamaro.nordigen

import com.gitlab.jdiasamaro.nordigen.model.AccountBalance
import com.gitlab.jdiasamaro.nordigen.model.AccountDetails
import com.gitlab.jdiasamaro.nordigen.model.AccountMetadata
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreement
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAcceptanceInfo
import com.gitlab.jdiasamaro.nordigen.model.EndUserAgreementAccessScope
import com.gitlab.jdiasamaro.nordigen.model.Institution
import com.gitlab.jdiasamaro.nordigen.model.NordigenAccessToken
import com.gitlab.jdiasamaro.nordigen.model.PaginatedEndUserAgreementList
import com.gitlab.jdiasamaro.nordigen.model.PaginatedRequisitionList
import com.gitlab.jdiasamaro.nordigen.model.RefreshedNordigenAccessToken
import com.gitlab.jdiasamaro.nordigen.model.Requisition
import com.gitlab.jdiasamaro.nordigen.model.TransactionsList

interface NordigenV2 {
    val tokens: Tokens
    val institutions: Institutions
    val endUserAgreements: EndUserAgreements
    val requisitions: Requisitions
    val accounts: Accounts

    interface Tokens {
        fun create(secretId: String, secretKey: String): NordigenAccessToken
        fun refresh(refreshToken: String): RefreshedNordigenAccessToken
    }

    interface Institutions {
        fun getAll(country: String? = null): List<Institution>
        fun get(institutionId: String): Institution
    }

    interface EndUserAgreements {
        fun getAll(limit: Int? = null, offset: Int? = null): PaginatedEndUserAgreementList
        fun get(agreementId: String): EndUserAgreement
        fun delete(agreementId: String)
        fun accept(agreementId: String): EndUserAgreementAcceptanceInfo
        fun create(
            institutionId: String,
            maxHistoricalDays: Int? = null,
            accessValidForDays: Int? = null,
            accessScopes: List<EndUserAgreementAccessScope> = emptyList()
        ): EndUserAgreement
    }

    interface Accounts {
        fun get(accountId: String): AccountMetadata
        fun getDetails(accountId: String): AccountDetails
        fun getBalances(accountId: String): List<AccountBalance>
        fun getTransactions(accountId: String): TransactionsList
    }

    interface Requisitions {
        fun getAll(limit: Int? = null, offset: Int? = null): PaginatedRequisitionList
        fun get(requisitionId: String): Requisition
        fun delete(requisitionId: String)
        fun create(
            institutionId: String,
            redirectUrl: String,
            reference: String? = null,
            userLanguage: String? = null,
            endUserAgreementId: String? = null
        ): Requisition
    }
}