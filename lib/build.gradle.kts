import org.gradle.api.JavaVersion.VERSION_11
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.31"
    `maven-publish`
    `java-library`
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://jitpack.io")
    }
}

java {
    sourceCompatibility = VERSION_11
    targetCompatibility = VERSION_11
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = VERSION_11.majorVersion
}

java {
    withJavadocJar()
    withSourcesJar()
}

tasks.create("currentVersion") {
    doLast {
        println(version())
    }
}

fun version() = "0.4.0"

publishing {
    publications {
        create<MavenPublication>("release") {
            group = "com.gitlab.jdiasamaro"
            artifactId = rootProject.name
            version = version()

            from(components["java"])
        }
    }
}

val versions = mapOf(
    "assertK"           to "0.25",
    "konnect"           to "0.21.0",
    "wiremock-kotlin"   to "1.0.2"
)

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.gitlab.jdiasamaro:konnect:${versions["konnect"]}")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:${versions["assertK"]}")
    testImplementation("com.marcinziolo:kotlin-wiremock:${versions["wiremock-kotlin"]}")
}
