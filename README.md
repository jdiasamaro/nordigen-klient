# Nordigen Klient

A Kotlin client for the Nordigen account information API v2.

## Quickstart Guide

Following the [official steps](https://nordigen.com/en/account_information_documenation/integration/quickstart_guide/), you can be up and running with `NordigenKlient`:

1. Create a new instance:

    ```kotlin
    val nordigen = NordigenKlient {
        secretId = "<secret-id-goes-here>"
        secretKey = "<secret-key-goes-here>"
    }
    ```

    By default, every instance will manage the access token automatically and renew them as needed (according to their expiry times). The only mandatory configuration parameters are the secret ID and key created on the Nordigen dashboard.


2. Choose an institution:

   ```kotlin
    val institutions = nordigen.v2.institutions.getAll(country = "<iso-3166-alpha-2>")
    ```

3. Create a default end-user agreement with one institution:

   ```kotlin
   val agreement = nordigen.v2.endUserAgreements.create(institutions.first().id)
   ```
   
4. Authenticate with the institution through a requisition link:

   ```kotlin
   val requisition = nordigen.v2.requisitions.create(
       redirectUrl = "https://www.after-oauth-is-completed.com",
       institutionId = "<institution-id>",
       endUserAgreementId = agreement.id
   )
   ```
   
   Follow the URL from `requisition.oauthAuthorizationUrl` and perform the authentication with the financial institution. Upon success, this request will be redirected to the provided `redirectUrl`.


5. That's it! Now the other API functions can be used, for instance:

    ```kotlin
   val myAccount = nordigen.v2.accounts.get(requisition.accountIds.first())
   val transactions = nordigen.v2.accounts.getTransactions(myAccount.id)
    
   val howMuchMoneySpent = transactions.booked.sumOf {
       it.amount < 0
   }
    ```

   Note: the account IDs can be stored for future reference. No need to pass through the requisition process again until the requisition expires.

## Dealing with access codes (authentication)

Nordigen requires valid access tokens to be sent with every API request. These oauth tokens need to be refreshed from time to time. `NordigenKlient` deals with this automatically:

1. When a first request is made, a new pair of access/refresh tokens are requested;
2. The access token is used until it expires. The refresh token is then used to get a new access token;
3. When the refresh token can no longer be used, a new access/refresh token pair is requested

### Custom access token management

`NordigenKlient` can be configured with a custom authentication provider when a different strategy to generate access tokens is necessary.

The following example provides a static access token without the need to expose the secret ID and key to the API client (if the token is obtained through other means). Please note that this static provider has limited functionality since the token is fixed. After it expires, API calls made with the configured `NordigenKlient` instance will return 401 UNAUTHORIZED.

It can also be used to have a more complex logic to provide access tokens.

```kotlin
val nordigen = NordigenKlient {
   authenticationProvider = onAccessTokenRequested {
      // can be completed with any logic to generate access codes
      "<static-access-token>"
   }
}
```

## Proxying API calls

`NordigenKlient` can be configured with a base endpoint to where API requests will be made. By default, the endpoint is `https://ob.nordigen.com/api/v2` but this can be configured with any HTTP endpoint. For instance, a proxy endpoint:

```kotlin
val nordigen = NordigenKlient {
   endpoint = "https://my-custom-proxy:5010"
}
```